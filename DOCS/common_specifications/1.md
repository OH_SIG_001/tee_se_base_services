# 前言


## 范围

本规范主要用于介绍OpenHarmony安全芯片管理框架、安全芯片中的高安服务功能规格以及对需要接入OpenHarmony安全芯片管理框架的安全芯片给出规范性定义，如安全性约束、芯片功能约束、Applet规格约束等，可以使各安全芯片厂商更容易接入到安全芯片管理框架的架构中，便于充分使用安全芯片能力和基于安全芯片开展业务创新，从而推动OpenHarmony安全芯片生态发展，使用安全芯片对OpenHarmony设备进行硬件级的安全防护。


## 用词约定

规则：必须遵守的功能要求

推荐：推荐遵守的功能要求

说明：对此约束/规则进行相应的解释说明

备注：对一些特殊情况进行说明描述


## 术语解释

**表1** 术语和定义

| 术语                              | 缩略语             | 中文解释                  |
| --------------------------------- | ------------------ | ------------------------- |
| Secure Element                    | SE                 | 安全元件                  |
| OpenHarmony Secure Element        | OpenHarmonySE/OHSE | OpenHarmony安全芯片的统称 |
| System on Chip                    | SoC                | 片上系统                  |
| Trusted Execution Environment     | TEE                | 可信执行环境              |
| Rich Execution Environment        | REE                | 富执行环境                |
| Root of Trust                     | ROT                | 可信根                    |
| Application Programming Interface | API                | 应用编程接口              |
| Application Protocol Data Unit    | APDU               | 应用协议数据单元          |
| NonVolatile Memory                | NVM                | 非易失性存储器            |
| True Random Number Generator      | TRNG               | 真随机数发生器            |
| Secure Channel Protocol           | SCP                | 安全通道协议              |
| Inter-Integrated Circuit          | I2C                | 内部集成电路              |
| Real-time Clock                   | RTC                | 安全实时时钟              |
| The Internet of Things            | IoT                | 物联网                    |
| PIN Authentication                | PINAuth            | 口令认证                  |
| SecureStorage                     | SS                 | 安全存储                  |
| Anti-rollback Protection          | ARP                | 防回滚保护                |
| Hardware                          | HW                 | 硬件                      |
| Firmware                          | FW                 | 固件                      |
| Software                          | SW                 | 软件                      |
| HarmonyUniversalKeyStore          | HUKS               | OpenHarmony通用密钥库系统 |
| Device Identifiers                | IDs                | 设备标识符                |
