include(GoogleTest)

add_executable(
    apdu_core_test
    src/apdu_utils_test.cpp
    src/card_channel_test.cpp
    src/command_apdu_test.cpp
    src/response_apdu_test.cpp
)

target_include_directories(apdu_core_test PRIVATE inc)
target_include_directories(apdu_core_test PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(apdu_core_test PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
target_link_libraries(apdu_core_test PRIVATE se_apdu_core_obj)
target_link_libraries(apdu_core_test PRIVATE logger_obj)
target_link_libraries(apdu_core_test PRIVATE se_base_services_defines)
target_link_libraries(apdu_core_test PRIVATE securec)
target_link_libraries(apdu_core_test PRIVATE se_base_test_mocks_obj)
target_link_libraries(apdu_core_test PRIVATE GTest::gmock_main)

gtest_discover_tests(apdu_core_test)
