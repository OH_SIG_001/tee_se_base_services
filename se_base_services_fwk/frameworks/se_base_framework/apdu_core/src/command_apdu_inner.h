/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CORE_SRC_COMMAND_APDU_INNER_H
#define CORE_SRC_COMMAND_APDU_INNER_H

#include "command_apdu.h"

typedef struct CommandApdu {
    uint32_t totalSize;
    ApduCommandHeader header;
    uint8_t body[0];
} CommandApdu;

#ifdef __cplusplus
extern "C" {
#endif

// in ISO/IEC 7816-3 command-response pairs case 1, no command data field, no response data field
CommandApdu *CreateCommandApduCaseOne(const ApduCommandHeader *header);

// in ISO/IEC 7816-3 command-response pairs case 2, no command data field, yes response data field
CommandApdu *CreateCommandApduCaseTwo(const ApduCommandHeader *header, uint32_t expectedLength);

// in ISO/IEC 7816-3 command-response pairs case 3, yes command data field, no response data field
CommandApdu *CreateCommandApduCaseThree(const ApduCommandHeader *header, const uint8_t *data, uint32_t dataLength);

// in ISO/IEC 7816-3 command-response pairs case 4, yes command data field, yes response data field
CommandApdu *CreateCommandApduCaseFour(const ApduCommandHeader *header, const uint8_t *data, uint32_t dataLength,
    uint32_t expectedLength);

#ifdef __cplusplus
}
#endif

#endif // CORE_SRC_COMMAND_APDU_INNER_H