/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CORE_INC_MSG_PARCEL_H
#define CORE_INC_MSG_PARCEL_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    uint8_t *data;
    uint32_t length;
    uint32_t beginPos;
    uint32_t endPos;
} Parcel;

Parcel CreateParcel(uint32_t size);
Parcel CreateParcelWithData(const uint8_t *data, uint32_t size);
void DeleteParcel(Parcel *parcel);

uint32_t GetParcelDataSize(const Parcel *parcel);
const uint8_t *GetParcelData(const Parcel *parcel);
bool ParcelToDataBuffer(const Parcel *parcel, uint8_t *buffer, uint32_t *bufferSize);

bool ParcelRead(Parcel *parcel, uint8_t *dst, uint32_t dataSize);
bool ParcelWrite(Parcel *parcel, const uint8_t *src, uint32_t dataSize);

bool ParcelWriteUint8(Parcel *parcel, uint8_t src);
bool ParcelWriteUint16(Parcel *parcel, uint16_t src);
bool ParcelWriteUint32(Parcel *parcel, uint32_t src);
bool ParcelWriteUint64(Parcel *parcel, uint64_t src);

bool ParcelReadUint8(Parcel *parcel, uint8_t *dst);
bool ParcelReadUint16(Parcel *parcel, uint16_t *dst);
bool ParcelReadUint32(Parcel *parcel, uint32_t *dst);
bool ParcelReadUint64(Parcel *parcel, uint64_t *dst);
#ifdef __cplusplus
}
#endif
#endif // CORE_INC_MSG_PARCEL_H
