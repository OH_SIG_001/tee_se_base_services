/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_STUB_SERIALIZATION_H
#define CODE_MODULES_INC_STORAGE_STUB_SERIALIZATION_H

#include "parcel.h"
#include "se_base_services_defines.h"
#include "se_module_sec_storage_defines.h"
#include "sec_storage_ipc_defines.h"
#include "sec_storage_ipc_stub.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode SetFactoryResetAuthKeyInputFromBuffer(const SharedDataBuffer *buffer, FactoryResetLevel *level,
    FactoryResetAuthAlgo *algo, StorageAuthKey *key);

ResultCode GetFactoryResetAuthKeyAlgoInputFromBuffer(const SharedDataBuffer *buffer, FactoryResetLevel *level);

ResultCode GetFactoryResetAuthKeyAlgoOutputToBuffer(FactoryResetAuthAlgo algo, SharedDataBuffer *buffer);

ResultCode PrepareFactoryResetOutputToBuffer(const uint8_t *nonce, uint32_t length, SharedDataBuffer *buffer);

ResultCode ProcessFactoryResetInputFromBuffer(const SharedDataBuffer *buffer, FactoryResetLevel *level,
    uint8_t *credential, uint32_t *length);

ResultCode SetToUserModeInputFromBuffer(const SharedDataBuffer *buffer, StorageUserModeConf *config);

ResultCode GetSlotOperateAlgorithmInputFromBuffer(const SharedDataBuffer *buffer, SlotOperAlgo *algo);

ResultCode GetSlotOperateAlgorithmOutputToBuffer(uint32_t available, SharedDataBuffer *buffer);

ResultCode GetFactoryResetAlgorithmInputFromBuffer(const SharedDataBuffer *buffer, FactoryResetAuthAlgo *algo);

ResultCode GetFactoryResetAlgorithmOutputToBuffer(uint32_t available, SharedDataBuffer *buffer);

ResultCode SetSetAllSlotsSizeInputFromBuffer(const SharedDataBuffer *buffer, uint16_t *slotSizeArray,
    uint32_t *arrayLength);

ResultCode GetAllSlotsSizeOutputToBuffer(const uint16_t *slotSizeArray, uint32_t arrayLength, SharedDataBuffer *buffer);

ResultCode AllocateSlotInputFromBuffer(const SharedDataBuffer *buffer, StorageFileName *name, StorageSlotAttr *slotAttr,
    StorageAuthKey *slotKey);

ResultCode WriteSlotInputFromBuffer(const SharedDataBuffer *buffer, StorageFileName *name, StorageAuthKey *key,
    StorageDataArea *area, StorageDataBuffer *data);

ResultCode ReadSlotInputFromBuffer(const SharedDataBuffer *buffer, StorageFileName *name, StorageAuthKey *key,
    StorageDataArea *area);

ResultCode ReadSlotOutputToBuffer(const StorageDataBuffer *data, SharedDataBuffer *buffer);

ResultCode FreeSlotInputFromBuffer(const SharedDataBuffer *buffer, StorageFileName *name, StorageAuthKey *key);

ResultCode GetSlotStatusInputFromBuffer(const SharedDataBuffer *buffer, StorageFileName *name);

ResultCode GetSlotStatusOutputToBuffer(const StorageSlotStatus *status, SharedDataBuffer *buffer);

bool ParcelReadStorageIndicator(Parcel *parcel, StorageFileName *name, StorageAuthKey *key, StorageDataArea *area);

bool ParcelToSharedDataBuffer(const Parcel *parcel, SharedDataBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_STUB_SERIALIZATION_H