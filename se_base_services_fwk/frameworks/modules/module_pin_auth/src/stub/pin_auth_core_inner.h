/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_PIN_AUTH_CORE_INNER_H
#define CODE_MODULES_INC_PIN_AUTH_CORE_INNER_H

#include "pin_auth_core.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PIN_AUTH_MAX_RETRY_NUM 1

enum {
    INS_PIN_ENROLL_LOCAL = 0x01,
    INS_PIN_ENROLL_REMOTE = 0x02,
    INS_PIN_AUTHENTICATION_LOCAL = 0x03,
    INS_PIN_AUTHENTICATION_REMOTE_PREPARE = 0x04,
    INS_PIN_AUTHENTICATION_REMOTE_PROCESS = 0x05,
    INS_PIN_AUTHENTICATION_REMOTE_ABORT = 0x06,
    INS_PIN_CONFIG_SELF_DESTRUCT = 0x07,
    INS_PIN_GET_SLOTS_NUM = 0x08,
    INS_PIN_GET_FREEZE_STATUS = 0x09,
    INS_PIN_CONFIG_FREEZE_POLICY = 0x0a,
    INS_PIN_GET_FREEZE_POLICY = 0x0b,
    INS_PIN_GET_ERASE_SINGLE_SLOT = 0x0c,
    INS_PIN_GET_ERASE_ALL_SLOT = 0x0d,
};

ResultCode PinAuthEnrollLocalFromRspApdu(const ResponseApdu *apdu, PinDataSecret *secret);

ResultCode PinAuthAuthenticationLocalFromRspApdu(const ResponseApdu *apdu, PinAuthResult *result);

ResultCode PinAuthGetNumSlotsFromRspApdu(const ResponseApdu *apdu, uint32_t *numSlots);

ResultCode PinAuthCmdSetSelfDestructEnableFromRspApdu(const ResponseApdu *apdu, PinConfigResult *configResult);

ResultCode PinAuthCmdSetSelfDestructEnableToCmdApduData(uint32_t slotId, const PinDestructConfig *config,
    const PinDataHash *hash, uint8_t *body, uint32_t *size);

ResultCode PinAuthGetFreezeStatusFormFromRspApdu(const ResponseApdu *apdu, uint32_t startIndex,
    PinFreezeStatus *status);

ResultCode PinAuthCmdSetFreezePolicyToCmdApduData(uint16_t punishStartCnt, uint16_t destructMaxCnt,
    uint32_t enableDestructDefault, uint8_t *body, uint32_t *size);

ResultCode PinAuthCmdSetFreezePolicyFromRspApdu(const ResponseApdu *apdu, uint8_t *status);

ResultCode PinAuthCmdGetFreezePolicyFromRspApdu(const ResponseApdu *apdu, uint16_t *punishStartCnt,
    uint16_t *destructMaxCnt, uint32_t *enableDestructDefault);

ResultCode ProcPinAuthCmdAuthRemotePrepareToCmdApduData(uint32_t slotId, uint64_t session, uint8_t *body,
    uint32_t *size);

ResultCode ProcPinAuthCmdAuthRemotePrepareFromRspApdu(const ResponseApdu *apdu,
    PinDataRemoteServiceChallenge *challenge);

ResultCode ProcPinAuthCmdAuthRemoteProcessToCmdApduData(uint32_t slotId, uint64_t session,
    const PinDataRemoteClientProof *proof, uint8_t *body, uint32_t *size);

ResultCode PinAuthAuthenticationRemoteFromRspApdu(const ResponseApdu *apdu, PinAuthResult *result);

ResultCode ProcPinAuthCmdAuthRemoteAbortToCmdApduData(uint32_t slotId, uint64_t session, uint8_t *body, uint32_t *size);

#ifdef __cplusplus
}
#endif
#endif // CODE_MODULES_INC_PIN_AUTH_CORE_INNER_H