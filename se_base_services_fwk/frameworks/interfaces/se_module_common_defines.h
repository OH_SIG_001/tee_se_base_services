/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTERFACES_MODULE_COMMON_DEFINES_H
#define INTERFACES_MODULE_COMMON_DEFINES_H

#include "se_base_services_defines.h"

#define SERVICE_STATUS_MAX_CONFIG_LEN 12
typedef struct SeServiceConfig {
    uint32_t vendor;
    uint8_t config[SERVICE_STATUS_MAX_CONFIG_LEN];
} SeServiceConfig;

#define SERVICE_STATUS_DATA_LEN 8
typedef struct SeServiceStatus {
    uint32_t available;
    uint32_t version;
    uint8_t status[SERVICE_STATUS_DATA_LEN];
    SeServiceConfig config;
} SeServiceStatus;

#endif // INTERFACES_MODULE_COMMON_DEFINES_H