add_executable(fuzz fuzz_main.cpp)
target_include_directories(fuzz PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})

target_compile_options(fuzz PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC} -fsanitize=fuzzer)
target_link_libraries(fuzz PRIVATE se_base_services_so)
target_link_libraries(fuzz PRIVATE se_base_services_defines)
target_link_libraries(fuzz PRIVATE securec)
target_link_libraries(fuzz PRIVATE tee_dyn_test_mocks)
target_link_libraries(fuzz PRIVATE -fsanitize=fuzzer)
