/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tee_service_public_mock.h"

extern "C" void tee_common_ipc_proc_cmd(const char *taskName, uint32_t sndCmd, const tee_service_ipc_msg *sndMsg,
    uint32_t ackCmd, tee_service_ipc_msg_rsp *rspMsg)
{
    auto stub = [](const char *taskName, uint32_t sndCmd, const tee_service_ipc_msg *sndMsg, uint32_t ackCmd,
                    tee_service_ipc_msg_rsp *rspMsg) -> void {
        // do default action
        (void)taskName;
        (void)sndCmd;
        (void)sndMsg;
        (void)ackCmd;
        rspMsg->ret = TEE_SUCCESS;
        rspMsg->msg.args_data.arg0 = 0;
    };

    if (auto *invoker = OHOS::SeBaseServices::UnitTest::TeeCommonIpcProcCmd::GetInstance(); invoker) {
        ON_CALL(*invoker, tee_common_ipc_proc_cmd).WillByDefault(stub);
        return invoker->tee_common_ipc_proc_cmd(taskName, sndCmd, sndMsg, ackCmd, rspMsg);
    }

    return stub(taskName, sndCmd, sndMsg, ackCmd, rspMsg);
}
