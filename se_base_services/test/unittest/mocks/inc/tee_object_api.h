/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_OBJECT_API_H
#define UNIT_TEST_INC_TEE_OBJECT_API_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include "tee_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

TEE_Result TEE_AllocateTransientObject(uint32_t objectType, uint32_t maxObjectSize, TEE_ObjectHandle *object);

void TEE_InitRefAttribute(TEE_Attribute *attr, uint32_t attributeID, uint8_t *buffer, uint32_t length);

void TEE_FreeTransientObject(TEE_ObjectHandle object);

TEE_Result TEE_PopulateTransientObject(TEE_ObjectHandle object, TEE_Attribute *attrs, uint32_t attrCount);

#ifdef __cplusplus
}
#endif

#endif // UNIT_TEST_INC_TEE_OBJECT_API_H